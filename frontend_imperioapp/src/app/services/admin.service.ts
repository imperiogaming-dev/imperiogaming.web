import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject, AsyncSubject } from 'rxjs';
import { Global } from './global';
import { User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  public url: string;
  public user: User;
  constructor(
    private HTTP: HttpClient
  ) {
    this.url = Global.url;
    this.user = new User('', '', '', '');
  }

  login(adminUser): Observable<any>{
    const params = JSON.stringify(adminUser);
    const header = new HttpHeaders().set('Content-Type', 'application/json');
    return this.HTTP.post(this.url + 'login', params, {headers : header});
  }
  loadGames(): Observable<any>{
    return this.HTTP.get(this.url + 'load-games');
  }
  loadCategories(data): Observable<any>{
    debugger
    return this.HTTP.get(this.url + 'load-categories/' + data);
  }
}
