import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AceptedComponent } from './acepted.component';

describe('AceptedComponent', () => {
  let component: AceptedComponent;
  let fixture: ComponentFixture<AceptedComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AceptedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceptedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
