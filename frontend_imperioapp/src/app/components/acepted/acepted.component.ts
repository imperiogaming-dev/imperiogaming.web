import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-acepted',
  templateUrl: './acepted.component.html',
  styleUrls: ['./acepted.component.css']
})
export class AceptedComponent implements OnInit {
  public id: any;
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
  }

}
