export class Categories{
    constructor(
        public id: string,
        public game_id: number,
        public image: string,
        public description: string,
        public name: string
    ){}
}
