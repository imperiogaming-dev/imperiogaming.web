-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-08-2020 a las 16:45:43
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `imperiogaming`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(20) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `game_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `image`, `game_id`) VALUES
(1, '1v1', 'uno contra unos dos', 'yasuo.jpg', 1),
(2, '5v5', 'uno contra todos', 'ave.jpg', 2),
(3, '1vE', 'uno contra ELLOS', 'ave.jpg', 1),
(4, '1vE', 'uno contra ELLOS', 'ave.jpg', 2),
(5, '1vE', 'uno contra ELLOS', 'ave.jpg', 2),
(6, '1vE', 'uno contra ELLOS', 'ave.jpg', 2),
(7, '1vE', 'uno contra ELLOS', 'ave.jpg', 3),
(8, '1vE', 'uno contra ELLOS', 'ave.jpg', 3),
(9, '1vE', 'uno contra ELLOS', 'ave.jpg', 3),
(10, '1vE', 'uno contra ELLOS', 'ave.jpg', 4),
(11, '1vE', 'uno contra ELLOS', 'ave.jpg', 4),
(12, '1vE', 'uno contra ELLOS', 'ave.jpg', 4),
(13, '1vE', 'uno contra ELLOS', 'ave.jpg', 5),
(14, '1vE', 'uno contra ELLOS', 'ave.jpg', 5),
(15, '1vE', 'uno contra ELLOS', 'ave.jpg', 5),
(16, '1vE', 'uno contra ELLOS', 'yasuo.jpg', 6),
(17, '1vE', 'uno contra ELLOS', 'yasuo.jpg', 6),
(18, '1vE', 'uno contra ELLOS', 'yasuo.jpg', 6),
(19, '1vE', 'uno contra ELLOS', 'yasuo.jpg', 2),
(20, '1vE', 'uno contra ELLOS', 'yasuo.jpg', 2),
(21, '1vE', 'uno contra ELLOS', 'yasuo.jpg', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `games`
--

CREATE TABLE `games` (
  `id` int(20) NOT NULL,
  `name` varchar(32) NOT NULL,
  `image` varchar(100) NOT NULL,
  `image_filter` varchar(100) NOT NULL,
  `image_tournament` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `games`
--

INSERT INTO `games` (`id`, `name`, `image`, `image_filter`, `image_tournament`) VALUES
(1, 'league of legends', 'ave.jpg', 'ave.jpg', 'ave.jpg'),
(2, 'hearthstones', 'ave.jpg', 'ave.jpg', 'ave.jpg'),
(3, 'valorant', 'yasuo.jpg', 'yasuo.jpg', 'yasuo.jpg'),
(4, 'counter strike', 'yasuo.jpg', 'yasuo.jpg', 'yasuo.jpg'),
(5, 'legends of runaterra', 'yasuo.jpg', 'yasuo.jpg', 'yasuo.jpg'),
(6, 'team fight tactics', 'yasuo.jpg', 'yasuo.jpg', 'yasuo.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `email` varchar(32) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`email`, `user_name`, `password`, `user_id`) VALUES
('nelesef', 'shinrateensseesse', '$2a$08$nHFva4.U7ef5fzwpd7U6C.cBB', 62),
('nelon', 'shinrateensseesse', '$2a$08$wY/FYlNtqV4BQvXn9177eexdBcAQlqk9t6iTAvuPTt.HIVntDJh/m', 63),
('nelonadmin', 'shinrateensseesse', '$2a$08$O3jCIe9PWWEG6gAjiszVMOQO3DqG2//C8lGCLi8i4hn0RTwZN.6f2', 64),
('nelonadmin2', 'shinrateensseesse', '$2a$08$e1MgQQZty7uQ70Pqjz5AOesbGYqrCRknn/CscWE0V8riskd7uXxz2', 65),
('nelonadmin3', 'shinrateensseesse', '$2a$08$NzBGdb2gkgCax5DnRbUEQ.O9BsRpXFBVBr68JfqXjtyJsBMQ0oxc2', 66),
('nelonadmin4', 'shinrateensseesse', '$2a$08$HqjvcwRLwEgW5tkViR3LY.h4FcZ8N4mYhnn5uHCpRPHMgcW0m7Ana', 67);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(20) NOT NULL,
  `name` varchar(32) NOT NULL,
  `image` varchar(32) NOT NULL,
  `game_id` int(20) NOT NULL,
  `points` int(20) NOT NULL,
  `stock` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `game_id`, `points`, `stock`) VALUES
(1, 'tarjeta de netflixdu', 'https://www.gettyimages.es/gi-re', 2, 60, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets`
--

CREATE TABLE `tickets` (
  `id` int(20) NOT NULL,
  `categorie` varchar(32) NOT NULL,
  `price` int(20) NOT NULL,
  `image` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tickets`
--

INSERT INTO `tickets` (`id`, `categorie`, `price`, `image`) VALUES
(1, '1', 8, 'https://i.pinimg.com/564x/43/59/'),
(2, '2', 5, 'https://i.pinimg.com/564x/43/59/'),
(3, '3', 8, 'https://i.pinimg.com/564x/43/59/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets_users`
--

CREATE TABLE `tickets_users` (
  `id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `ticket_id` int(20) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tickets_users`
--

INSERT INTO `tickets_users` (`id`, `user_id`, `created_at`, `ticket_id`, `active`) VALUES
(3, 63, '2020-08-24 22:05:58.236044', 3, 0),
(4, 63, '2020-08-24 22:06:07.328629', 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tournaments`
--

CREATE TABLE `tournaments` (
  `id` int(20) NOT NULL,
  `game_id` int(20) NOT NULL,
  `name` varchar(32) NOT NULL,
  `categorie_id` int(20) NOT NULL,
  `url_GT` varchar(32) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `end_at` timestamp(6) NULL DEFAULT NULL,
  `image` varchar(32) NOT NULL,
  `rules` varchar(32) NOT NULL,
  `creator_id` int(20) NOT NULL,
  `users_capacity` int(20) NOT NULL,
  `first_place` int(20) NOT NULL DEFAULT 0,
  `second_place` int(20) NOT NULL DEFAULT 0,
  `third_place` int(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tournaments`
--

INSERT INTO `tournaments` (`id`, `game_id`, `name`, `categorie_id`, `url_GT`, `created_at`, `end_at`, `image`, `rules`, `creator_id`, `users_capacity`, `first_place`, `second_place`, `third_place`) VALUES
(1, 1, 'torunaDos3', 1, 'https://www.gettyimages.es/gi-re', '2020-08-18 21:55:15.218850', '2020-08-20 23:09:19.000000', 'https://www.gettyimages.es/gi-re', 'https://www.gettyimages.es/gi-re', 66, 16, 20, 10, 5),
(2, 1, 'torunaDos', 1, 'https://www.gettyimages.es/gi-re', '2020-08-18 22:04:45.481980', '2020-08-20 23:09:19.000000', 'https://www.gettyimages.es/gi-re', 'https://www.gettyimages.es/gi-re', 66, 20, 20, 10, 5),
(3, 2, 'torunaDos', 2, 'https://www.gettyimages.es/gi-re', '2020-08-24 20:55:58.177352', '2020-08-20 23:09:19.000000', 'https://www.gettyimages.es/gi-re', 'https://www.gettyimages.es/gi-re', 63, 20, 20, 10, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tournaments_subscriptions`
--

CREATE TABLE `tournaments_subscriptions` (
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `tournament_id` int(20) NOT NULL,
  `ticket_users_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tournaments_subscriptions`
--

INSERT INTO `tournaments_subscriptions` (`created_at`, `tournament_id`, `ticket_users_id`) VALUES
('2020-08-24 22:05:58.237115', 1, 3),
('2020-08-24 22:08:05.376993', 2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transactions`
--

CREATE TABLE `transactions` (
  `id` int(20) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `user_id` int(20) NOT NULL,
  `product_id` int(20) NOT NULL,
  `state` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `transactions`
--

INSERT INTO `transactions` (`id`, `created_at`, `user_id`, `product_id`, `state`) VALUES
(4, '2020-08-20 00:53:10.856042', 63, 1, 'pendiente'),
(5, '2020-08-20 00:58:22.677608', 63, 1, 'pendiente'),
(6, '2020-08-20 00:59:56.328053', 63, 1, 'pendiente'),
(7, '2020-08-20 01:04:16.192646', 63, 1, 'pendiente'),
(8, '2020-08-20 01:06:03.165089', 63, 1, 'pendiente'),
(9, '2020-08-20 01:08:43.269356', 63, 1, 'pendiente'),
(10, '2020-08-20 01:09:14.486532', 63, 1, 'pendiente'),
(11, '2020-08-20 01:09:35.262143', 63, 1, 'pendiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `user_type` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `created_At` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `full_name`, `country_code`, `user_type`, `email`, `created_At`) VALUES
(62, 'Nelson Araujo', 'CO', 'user', 'nelesef', '2020-08-10 21:09:19'),
(63, 'Nelson Araujo', 'CO', 'user', 'nelon', '2020-08-10 21:33:31'),
(64, 'Nelson Araujo', 'CO', 'admin', 'nelonadmin', '2020-08-12 04:34:58'),
(65, 'Nelson Araujo', 'CO', 'admin', 'nelonadmin2', '2020-08-12 04:38:34'),
(66, 'Nelson Araujo', 'CO', 'admin', 'nelonadmin3', '2020-08-12 04:42:37'),
(67, 'Nelson Araujo', 'CO', 'admin', 'nelonadmin4', '2020-08-12 04:44:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_points`
--

CREATE TABLE `user_points` (
  `user_id` int(20) NOT NULL,
  `points` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_points`
--

INSERT INTO `user_points` (`user_id`, `points`) VALUES
(62, 200),
(63, 1400);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `game_id` (`game_id`);

--
-- Indices de la tabla `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`email`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `game_id` (`game_id`);

--
-- Indices de la tabla `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tickets_users`
--
ALTER TABLE `tickets_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `ticket_id` (`ticket_id`);

--
-- Indices de la tabla `tournaments`
--
ALTER TABLE `tournaments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `game_id` (`game_id`),
  ADD KEY `creator_id` (`creator_id`),
  ADD KEY `categorie_id` (`categorie_id`);

--
-- Indices de la tabla `tournaments_subscriptions`
--
ALTER TABLE `tournaments_subscriptions`
  ADD UNIQUE KEY `ticket_users_id_2` (`ticket_users_id`),
  ADD KEY `tournament_id` (`tournament_id`),
  ADD KEY `ticket_users_id` (`ticket_users_id`);

--
-- Indices de la tabla `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `user_points`
--
ALTER TABLE `user_points`
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `games`
--
ALTER TABLE `games`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tickets_users`
--
ALTER TABLE `tickets_users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tournaments`
--
ALTER TABLE `tournaments`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tickets_users`
--
ALTER TABLE `tickets_users`
  ADD CONSTRAINT `tickets_users_ibfk_1` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tickets_users_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tournaments`
--
ALTER TABLE `tournaments`
  ADD CONSTRAINT `tournaments_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tournaments_ibfk_2` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tournaments_ibfk_3` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tournaments_subscriptions`
--
ALTER TABLE `tournaments_subscriptions`
  ADD CONSTRAINT `tournaments_subscriptions_ibfk_2` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tournaments_subscriptions_ibfk_3` FOREIGN KEY (`ticket_users_id`) REFERENCES `tickets_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user_points`
--
ALTER TABLE `user_points`
  ADD CONSTRAINT `user_points_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
