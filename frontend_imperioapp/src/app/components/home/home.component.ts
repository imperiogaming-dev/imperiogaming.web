import { Component, OnInit } from '@angular/core';
declare var parJS: any;
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public particle: any;
  public executed = false;
  constructor(
    private window: Window,
  ) {
  }

  ngOnInit(): void {
    if (!this.executed){
      if (this.window.innerWidth >= 786){
        this.particle = parJS('particles-js',
        {
          "particles":{
            "number":{
              "value":300,
              "density":{
                "enable":true,
                "value_area":3000
              }
            },
            "color":{
              "value":"#FFE900"
            },
            "shape":{
              "type":"circle",
              "stroke":{
                "width":0,
                "color":"#000000"
              },
              "polygon":{
                "nb_sides":3
              },
              "image":{
                "src":"img/github.svg"
                ,"width":200,
                "height":200
              }
            },
            "opacity":{
              "value":0.8,
              "random":true,
              "anim":{
                "enable":false,
                "speed":0.4,
                "opacity_min":0.3,
                "sync":false
              }
            },
            "size":{
              "value":4,
              "random":true,
              "anim":{
                "enable":true,
                "speed":5,
                "size_min":0,
                "sync":false
              }
            },
            "line_linked":{
              "enable":false,
              "distance":500,
              "color":"#ffffff",
              "opacity":0.4,
              "width":2},
              "move":{
                "enable":true,
                "speed":7.8914764163227265,
                "direction":"top",
                "random":true,
                "straight":false,
                "out_mode":"out",
                "bounce":false,
                "attract":{
                  "enable":false,
                  "rotateX":600,
                  "rotateY":1200
                }
              }
            },
            "interactivity":{
              "detect_on":"canvas",
              "events":{
                "onhover":{
                  "enable":false,
                  "mode":"bubble"
                },
                "onclick":{
                  "enable":false,
                  "mode":"repulse"
                },
                "resize":true
              },
              "modes":{
                "grab":{
                  "distance":400,
                  "line_linked":{
                    "opacity":0.5
                  }
                },
                "bubble":{
                  "distance":400,
                  "size":8,
                  "duration":0.3,
                  "opacity":1,
                  "speed":3
                },
                "repulse":{
                  "distance":200,
                  "duration":0.4
                },
                "push":{
                  "particles_nb":4
                },
                "remove":{
                  "particles_nb":2
                }
              }
            }
        },
         this.window);
      }
      this.executed = true;
    }
  }
}
