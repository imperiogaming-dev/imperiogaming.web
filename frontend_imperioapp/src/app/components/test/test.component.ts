import { Component, OnInit, Renderer2 } from '@angular/core';
import { Global } from '../../services/global';
import { EpaycoService } from 'src/app/services/epayco.service';
declare const ePayco: any;

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  public url = Global;
  public handler: any;
  public data: any;
  constructor(
    private ePaycoService: EpaycoService,
    private renderer2: Renderer2,
  ) { }

  ngOnInit(): void {
    const s = this.renderer2.createElement('script');
    s.type = 'text/javascript';
    s.onload = this.loadNextScript.bind(this);
    s.src = 'https://checkout.epayco.co/checkout.js';
    s.text = ``;
    this.renderer2.appendChild(document.body, s);
  }
  loadNextScript(){
    const s = this.renderer2.createElement('script');
    s.text = `
    handler = ePayco.checkout.configure({
      key: '53a4ecd91d6f45528079f84c514a248d',
      test: true
    });
    const data = {
      //Parametros compra (obligatorio)
      name: "Tickete de mierdadd",
      description: "Nekochan ",
      invoice: "123451",
      currency: "cop",
      amount: "11900",
      tax_base: "10000",
      tax: "1900",
      country: "co",
      lang: "en",
      external: "false",


      //Atributos opcionales
      extra1: "extra1",
      extra2: "extra2",
      extra3: "extra3",
      acepted: "http://localhost:4200/acepted/123",//mandar variables por url el id del producto
      rejected: "http://localhost:4200/error",
      pending: "http://localhost:4200/pending"
    };
    /*//Atributos cliente
       name_billing: "Andres Perez",
      address_billing: "Carrera 19 numero 14 91",
      type_doc_billing: "cc",
      mobilephone_billing: "3050000000",
      number_doc_billing: "100000000",
      */
    var btn=document.getElementById('btn-epayco');
    btn.onclick = function(){handler.open(data)};`;
    this.renderer2.appendChild(document.body, s);
  }
}
