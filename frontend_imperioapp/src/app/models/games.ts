export class Games{
    constructor(
        public id: string,
        public image: string,
        public image_filter: string,
        public image_tournament: string,
        public name: string
    ){}
}
