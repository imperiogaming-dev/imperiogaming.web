import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { AdminService } from 'src/app/services/admin.service';
import { Games } from 'src/app/models/games';
import { Categories } from 'src/app/models/categories';
import { Global } from 'src/app/services/global';

declare var $: any;
@Component({
  selector: 'app-tournaments',
  templateUrl: './tournaments.component.html',
  styleUrls: ['./tournaments.component.css']
})
export class TournamentsComponent implements OnInit {
  public games: Games[];
  public list: any;
  public categories: Categories[];
  public url: string;
  public cat: boolean;
  constructor(
    private renderer2: Renderer2,
    private ADMINSERVICE: AdminService,
  ) {
    this.url = Global.url;
    this.cat = false;
  }

  ngOnInit(): void {
    this.ADMINSERVICE.loadGames().subscribe(
      response => {
        if ( response.status === 'Ok'){
          this.games = response.data;
          //const userInDB = response.data[0];
          const s = this.renderer2.createElement('script');
          s.type = 'text/javascript';
          s.text = `
          $(document).ready(function() {
            var slider = $('#autoWidth').lightSlider({
                autoWidth:true,
                loop:true,
                controls:false,
                onSliderLoad: function() {
                    $('#autoWidth').removeClass('cS-hidden');
                }
            });
            $('#goToPrevSlide').on('click', function () {
              slider.goToPrevSlide();
            });
            $('#goToNextSlide').on('click', function () {
                slider.goToNextSlide();
            });
          });
         `;
          this.renderer2.appendChild(document.body, s);
        }else{
          alert('Nonas: ');
        }
      },
      error => {
        console.log(error);
      }
    );
  }
  loadNextScript(){
    const s = this.renderer2.createElement('script');
    s.text = `
    $(document).ready(function() {
      $('#autoWidth').lightSlider({
          autoWidth:true,
          loop:true,
          onSliderLoad: function() {
              $('#autoWidth').removeClass('cS-hidden');
          }
      });
    });`;
    this.renderer2.appendChild(document.body, s);
  }
  onClickGame(data){
    const game = data.currentTarget.value;
    this.ADMINSERVICE.loadCategories(game).subscribe(
      response => {
        if ( response.status === 'Ok'){
          this.categories = response.data;
          if (!this.cat){
            this.cat = true;
            const s = this.renderer2.createElement('script');
            s.type = 'text/javascript';
            s.text = `
              $(document).ready(function() {
                var slider = $('#autoWidths').lightSlider({
                    autoWidth:true,
                    loop:true,
                    controls:false,
                    pager: false,
                    onSliderLoad: function() {
                        $('#autoWidths').removeClass('cS-hidden');
                    }
                });
                $('#goToPrevSlider').on('click', function () {
                  slider.goToPrevSlide();
                });
                $('#goToNextSlider').on('click', function () {
                    slider.goToNextSlide();
                });
              });
            `;
            this.renderer2.appendChild(document.body, s);
          }else{
           /* const s = this.renderer2.createElement('script');
            s.type = 'text/javascript';
            s.text = `
              $(document).ready(function() {
                var slider = $('#autoWidths').lightSlider({
                    autoWidth:true,
                    loop:true,
                    controls:false,
                    pager: false,
                    onSliderLoad: function() {
                        $('#autoWidths').removeClass('cS-hidden');
                    }
                });
              });
            `;
            this.renderer2.appendChild(document.body, s);*/
          }
        }else{
          alert('Nonas: ');
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
